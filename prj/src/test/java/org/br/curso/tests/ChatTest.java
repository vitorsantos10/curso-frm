package org.br.curso.tests;

import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static br.com.santander.frm.bdd.Gherkin.executeScenario_;

import br.com.santander.frm.base.TestBase;
import br.com.santander.frm.controllers.WebController;
import br.com.santander.frm.testng.DataTableConfig;
import br.com.santander.frm.testng.TestConfig;

@TestConfig(controllerType = WebController.class)
public class ChatTest extends TestBase {

	@Test(groups = { "Chat" }, priority = 1, testName = "CT004-Chat")
	public void executar_login_valido() {
		try {
			executeScenario_("Chat", "CT004-Chat");
		} catch (Exception e) {
			Assert.fail("Test error ", e);
		}
	}

	@BeforeMethod(alwaysRun = true)
	public void setup(final Method method, final ITestContext context) {
		super.setup(method, context);
	}

	@AfterMethod(alwaysRun = true)
	public void teardawon(final Method method, final ITestContext context, ITestResult testResult) {
		super.teardown(method, context, testResult);
	}
}
