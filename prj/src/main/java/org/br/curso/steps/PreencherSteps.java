package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.LoginPage;
import org.br.curso.pages.PreencherPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class PreencherSteps {

	HomePage homePage = getPage_(HomePage.class);
	PreencherPage preencherPage = getPage_(PreencherPage.class);
	LoginPage loginPage = getPage_(LoginPage.class);

	@Step("Dado que estou na pagina account")
	public void Dado_que_estou_na_pagina_account() throws ElementFindException {

		homePage.validar_account();
		homePage.clicar_em_account();

	}

	@Step("Efetuar o preenchimento")
	public void Efetuar_o_preenchimento() throws ElementFindException {
		preencherPage.preencher_formulario("vitor", "santos", "vitoaarhotmail", "santossantos", "12345", "",
				"rua lagoa da tocha", null);

	}

	@Step("Ent�o o cadastro ser� realizado e validado")
	public void Ent�o_o_cadastro_ser�_realizado_e_validado() throws ElementFindException {
		preencherPage.validarCadastroCliente();
		preencherPage.validar_update();
		
	}

}
