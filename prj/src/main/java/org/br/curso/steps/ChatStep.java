package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.LoginPage;
import org.br.curso.pages.PreencherPage;
import org.br.curso.pages.MensagemPage;
import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps

public class ChatStep {

	HomePage homePage = getPage_(HomePage.class);
	PreencherPage preencherPage = getPage_(PreencherPage.class);
	LoginPage loginPage = getPage_(LoginPage.class);
	MensagemPage mensagemPage = getPage_(MensagemPage.class);

	@Step("Abrir Chat")
	public void Abrir_Chat() throws ElementFindException {
		homePage.validar_chat();
		homePage.clicar_chat();
		homePage.validar_Welcome();

	}

	@Step("Interagir com Chat")
	public void Interagir_com_Chat() throws ElementFindException {
		homePage.validar_start_chat();
		homePage.clicar_start();
	}
}
