package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class HomeSteps {

	HomePage homePage = getPage_(HomePage.class);

	@Step("Ent�o a home ser� exibida")
	public void entao_a_home_ser�_exibida() throws ElementFindException {
		homePage.validar_pagina();
	}
}
