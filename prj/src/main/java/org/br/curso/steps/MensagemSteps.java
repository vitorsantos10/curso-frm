package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.LoginPage;
import org.br.curso.pages.PreencherPage;
import org.br.curso.pages.MensagemPage;
import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class MensagemSteps {

	HomePage homePage = getPage_(HomePage.class);
	PreencherPage preencherPage = getPage_(PreencherPage.class);
	LoginPage loginPage = getPage_(LoginPage.class);
	MensagemPage mensagemPage = getPage_(MensagemPage.class);

	@Step("ir a pagina vetor ")
	public void ir_a_pagina_vetor() throws ElementFindException {
		preencherPage.clicar_em_viator();
		mensagemPage.validar_pagina();

	}

	@Step("Ent�o configurar vetor")
	public void Ent�o_configurar_vetor() throws ElementFindException {
		mensagemPage.op��es_api();
		mensagemPage.clicar_submit();
		mensagemPage.validar_troca();
		mensagemPage.voltar_home();
		homePage.validar_pagina();
	}

}
