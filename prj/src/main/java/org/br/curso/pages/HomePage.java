package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class HomePage extends PageBase {

	VirtualElement lblDashboard = getElementByXPath("//p[text() = 'DASHBOARD']"),
			addPage = getElementByXPath("//li[@id='account']"), lnkAccount = getElementByXPath("//li[@id='account']"),
			lblProfile = getElementByXPath("//li[@id='account']"), chat = getElementByXPath("//span[@class = 'Linkify']"),
			welcomeChat = getElementByXPath("//h1[@class = 'lc-1oputd8 e7bf83j1']"),
			startChat = getElementByXPath("//button[@class = 'lc-3a63hl esv0owm0']"),
			texWel = getElementByXPath("//div[@class = 'lc-4g9muy e12liu9t0']");

	LoggerHelper logger = new LoggerHelper(HomePage.class);

	public void validar_pagina() {

		Assert.assertTrue(elementExists(lblDashboard));
		logger.info("P�gina inicial foi encontrada.", true);

	}

	public void clicar_em_account() throws ElementFindException {
		lnkAccount.click();
	}

	public void validar_account() {
		Assert.assertTrue(elementExists(lblProfile));
		logger.info("Pagina Account encontrada");
	}

	public void validar_chat() {
		Assert.assertTrue(elementExists(chat));
		logger.info("Pagina Account encontrada");
	}

	public void clicar_chat() throws ElementFindException {
		chat.click();
	}

	public void validar_Welcome() {
		Assert.assertTrue(elementExists(welcomeChat));
		logger.info("Pagina Account encontrada");
	}

	public void validar_start_chat() {
		Assert.assertTrue(elementExists(startChat));
		logger.info("bot�o de chat encontrado");
	}

	public void clicar_start() throws ElementFindException {
		startChat.click();
	}

	public void validar_text_wel() {
		Assert.assertTrue(elementExists(texWel));
		logger.info("bot�o de chat encontrado");
	}
}