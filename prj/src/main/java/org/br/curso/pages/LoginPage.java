package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;
import br.com.santander.frm.base.*;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class LoginPage extends PageBase {

	VirtualElement txtUsuario = getElementByXPath("//span[text()='Email Address']/.."),
			txtSenha = getElementByXPath("//input[@name = 'password']"),
			btnLogin = getElementByXPath("//span[text()='Login']/.."),
			pnlLogin = getElementByXPath("//strong[text()='Login Panel']");

	LoggerHelper logger = new LoggerHelper(LoginPage.class);

	public void validar_pagina() {

		Assert.assertTrue(elementExists(pnlLogin));
		logger.info("P�gina de login foi encontrada.", true);

	}

	public void preencher_login(String usuario, String senha) throws ElementFindException {

		if (elementExists(txtUsuario) && elementExists(txtSenha) && elementExists(btnLogin)) {
			txtUsuario.sendKeys(usuario);
			txtSenha.sendKeys(senha);

			logger.info("Campos da p�ginda de login preenchidos", true);

			btnLogin.click();

		} else {
			logger.error("N�o foi feito corretamente o carregamento da p�gina!", true);
		}
	}

}
