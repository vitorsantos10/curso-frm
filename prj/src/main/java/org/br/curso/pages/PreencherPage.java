package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class PreencherPage extends PageBase {
	VirtualElement lblCustomersManagement = getElementByXPath("//div[@class = 'panel-heading']"),
			dashbord = getElementByXPath("//strong[text()='Dashboard']"),
			txtFirstName = getElementByXPath("//input[@name = 'fname']"),
			txtLastname = getElementByXPath("//input[@name = 'fname']"),
			txtEmail = getElementByXPath("//input[@name = 'email']"),
			txtPassoword = getElementByXPath("//input[@name = 'password']"),
			txtMobile = getElementByXPath("//input[@name = 'mobile']"),
			txtAddress1 = getElementByXPath("//input[@name = 'address1']"),
			txtAddress2 = getElementByXPath("//input[@name = 'address2']"),
			ckbEmail = getElementByXPath("//input[@name = 'email']"),
			btnSubmit = getElementByXPath("//button[text() = 'Submit']"),
			CadastroCliente = getElementByXPath("//h4[@class = 'ui-pnotify-title']"), // Barra verde que mostra que o
																						// usuario foi cadastrado
			Viator = getElementByXPath("//a[@href = '#Viator']"),
	        Viator2 = getElementByXPath("//ul[@id = 'Viator']");

	LoggerHelper logger = new LoggerHelper(LoginPage.class);

	public void validar_campos() {

		Assert.assertTrue(elementExists(lblCustomersManagement));
		logger.info("P�gina de login foi encontrada.", true);

	}

	public void preencher_formulario(String firstName, String lastName, String Email, String password, String mobile,
			String coutry, String address1, String address2) throws ElementFindException {
		if (elementExists(txtFirstName) && elementExists(txtLastname) && elementExists(txtEmail)
				&& elementExists(txtPassoword) && elementExists(txtMobile) && elementExists(txtAddress1)
				&& elementExists(txtAddress2)) {

			txtFirstName.sendKeys(firstName);
			txtLastname.sendKeys(lastName);
			ckbEmail.click();
			txtEmail.sendKeys(Email);
			txtPassoword.sendKeys(password);
			txtMobile.sendKeys(mobile);
			txtAddress1.click();
			txtAddress2.click();

			logger.info("Campos preenchidos", true);

			btnSubmit.click();

		} else {
			logger.error("Os campos n�o foram preenchidos e o Usuario n�o foi cadastrado!", true);
		}

	}

	public void validarCadastroCliente() {
		Assert.assertTrue(elementExists(CadastroCliente));
		logger.info("Usuario foi Cadastrado.", true);

	}

	public void validar_update() { // validar recarregamento da pagina.

		Assert.assertTrue(elementExists(dashbord));
		logger.info("P�gina de login foi encontrada.", true);
	}

	public void clicar_em_viator() throws ElementFindException {
		Viator.click();
        Viator2.click();
	}

}
