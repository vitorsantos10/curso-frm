package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class MensagemPage extends PageBase {
	VirtualElement homedashboard = getElementByXPath("//p[text() = 'DASHBOARD']"),
			ViatorHome = getElementByXPath("//span[@class = 'panel-title pull-left']"),
			btnSubmit = getElementByXPath("//i[@class = 'fa fa-save']"),
			Api = getElementByXPath("//select[@name = 'api_environment']"),
			sandboxApi = getElementByXPath("//option[@value = 'sandbox']"),
			dashboard = getElementByXPath("//strong[text() = 'Dashboard']");

	LoggerHelper logger = new LoggerHelper(LoginPage.class);

	public void validar_pagina() {
		Assert.assertTrue(elementExists(ViatorHome));
		logger.info("Pagina foi encontrada.", true);

	}

	public void op��es_api() throws ElementFindException {
		Api.click();
		sandboxApi.click();

	}

	public void clicar_submit() throws ElementFindException {
		btnSubmit.click();
	}

	public void validar_troca() {
		Assert.assertTrue(elementExists(sandboxApi));
		logger.info("a troca foi feita.", true);

	}

	public void voltar_home() throws ElementFindException {
		dashboard.click();
	}
}
