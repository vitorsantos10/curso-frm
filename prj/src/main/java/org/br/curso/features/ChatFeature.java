package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Chat")
public class ChatFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT004-Chat")
	public void Enviar_Mensagem() throws ExecutionException {

		given_("Dado que estou na p�gina do phpwebtravels").when_("Quando efetuar o login com usuario valido")
				.and_("Dado que estou na pagina account").and_("Efetuar o preenchimento")
				.and_("Ent�o o cadastro ser� realizado e validado").and_("ir a pagina vetor ")
				.and_("Ent�o configurar vetor").and_("Abrir Chat").then_("Interagir com Chat").execute_();

	}

}
