package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Preencher")
public class PreencherFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT002-Cadastro")
	public void inserir_informacoes() throws ExecutionException {

		given_("Dado que estou na p�gina do phpwebtravels").when_("Quando efetuar o login com usuario valido")
				.and_("Dado que estou na pagina account").and_("Efetuar o preenchimento")
				.then_("Ent�o o cadastro ser� realizado e validado").execute_();

	}

}
