package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Login")
public class LoginFeature {

	@SuppressWarnings("static-access")
	@Scenario("Executar login com usuario v�lido")
	public void executar_login_valido() throws ExecutionException {
		given_("Dado que estou na p�gina do phpwebtravels").when_("Quando efetuar o login com usuario valido")
				.then_("Ent�o a home ser� exibida").execute_();
	}

}
