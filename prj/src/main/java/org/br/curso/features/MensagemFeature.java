package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Mensagem")
public class MensagemFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT003-Mensagem")
	public void Enviar_Mensagem() throws ExecutionException {

		given_("Dado que estou na p�gina do phpwebtravels").when_("Quando efetuar o login com usuario valido")
				.and_("Dado que estou na pagina account").and_("Efetuar o preenchimento")
				.and_("Ent�o o cadastro ser� realizado e validado").and_("ir a pagina vetor ")
				.then_("Ent�o configurar vetor").execute_();

	}

}
